package com.example.cekongkir;

public class dataBookmark {

    private String asal;
    private String tujuan;
    private Integer berat;

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public Integer getBerat() {
        return berat;
    }

    public void setBerat(Integer berat) {
        this.berat = berat;
    }

    public dataBookmark(String asal, String tujuan, Integer berat) {
        this.asal = asal;
        this.tujuan = tujuan;
        this.berat = berat;
    }
}

package com.example.cekongkir;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;


import com.example.cekongkir.databinding.ActivityMenuBinding;
import com.example.cekongkir.ui.home.MainActivity;

public class ActivityMenu extends AppCompatActivity {
       private ActivityMenuBinding menuBinding;


//    ImageView iv_notif;
//    LinearLayout lacak, scan, bookmark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().hide();

        menuBinding = DataBindingUtil.setContentView(this,R.layout.activity_menu);

//        iv_notif=findViewById(R.id.iv_notif);
//        lacak=findViewById(R.id.lacak);
//        scan=findViewById(R.id.scan);
//        bookmark=findViewById(R.id.bookmark);
        menuBinding.ivNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tonotif= new Intent(ActivityMenu.this, ActivityNotifikasi.class);
                startActivity(tonotif);
            }
        });

        menuBinding.lacak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tolacak= new Intent(ActivityMenu.this, ActivityLacak.class);
                startActivity(tolacak);
            }
        });

        menuBinding.scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toscan= new Intent(ActivityMenu.this, ActivityScan.class);
                startActivity(toscan);
            }
        });

        menuBinding.ongkir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toongkir= new Intent(ActivityMenu.this, MainActivity.class);
                startActivity(toongkir);
            }
        });

        menuBinding.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tobookmark= new Intent(ActivityMenu.this, ActivityBookmark.class);
                startActivity(tobookmark);

            }
        });

        menuBinding.infokurir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toinfo= new Intent(ActivityMenu.this, ActivityInfoKurir.class);
                startActivity(toinfo);
            }
        });

        menuBinding.btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoUrl("https://play.google.com/");
            }
        });

    }

    private void gotoUrl(String s) {
        Uri uri = Uri.parse(s);
        startActivity(new Intent(Intent.ACTION_VIEW,uri));

    }
}
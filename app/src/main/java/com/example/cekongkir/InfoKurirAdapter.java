package com.example.cekongkir;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class InfoKurirAdapter extends RecyclerView.Adapter<InfoKurirAdapter.InfoKurirViewHolder> {

    private ArrayList<DataInfoKurir> infoKurirList;

    public InfoKurirAdapter(ArrayList<DataInfoKurir> infoKurirList) {
        this.infoKurirList = infoKurirList;
    }

    @NonNull
    @Override
    public InfoKurirAdapter.InfoKurirViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_infokurir, parent, false);
        return new InfoKurirViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InfoKurirAdapter.InfoKurirViewHolder holder, int position) {
        holder.IconKurir.setImageResource(infoKurirList.get(position).getLogo());
        holder.Call.setText(infoKurirList.get(position).getCall());
        holder.Email.setText(infoKurirList.get(position).getEmail());
        holder.Facebook.setText(infoKurirList.get(position).getFacebook());
        holder.Twitter.setText(infoKurirList.get(position).getTwitter());
        holder.Instagram.setText(infoKurirList.get(position).getInstagram());
    }

    @Override
    public int getItemCount() {
        return (infoKurirList!= null) ? infoKurirList.size() : 0;
    }

    public class InfoKurirViewHolder extends RecyclerView.ViewHolder {

        private ImageView IconKurir;
        private TextView Call, Email, Facebook, Twitter, Instagram;

        public InfoKurirViewHolder (View view){
            super(view);

            IconKurir = view.findViewById(R.id.img_logokurir);
            Call = view.findViewById(R.id.tv_call);
            Email = view.findViewById(R.id.tv_email);
            Facebook = view.findViewById(R.id.tv_facebook);
            Twitter = view.findViewById(R.id.tv_twitter);
            Instagram = view.findViewById(R.id.tv_instagram);

        }
    }
}

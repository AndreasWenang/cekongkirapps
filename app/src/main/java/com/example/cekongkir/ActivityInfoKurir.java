package com.example.cekongkir;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class ActivityInfoKurir extends AppCompatActivity {

    private RecyclerView recyclerView;
    private InfoKurirAdapter infoKurirAdapter;
    private ArrayList<DataInfoKurir> infoKurirArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_kurir);

        getData();

        recyclerView = findViewById(R.id.infkurirrecycler);

        infoKurirAdapter = new InfoKurirAdapter(infoKurirArrayList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ActivityInfoKurir.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(infoKurirAdapter);
    }

    public void getData(){
        infoKurirArrayList = new ArrayList<>();
        DataInfoKurir kurir = new DataInfoKurir();
        kurir.setLogo(R.mipmap.logo_jne);
        kurir.setCall("021-29278888");
        kurir.setEmail("customercare@jne.co.id");
        kurir.setFacebook("@JNEPusat");
        kurir.setTwitter("@JNE_ID");
        kurir.setInstagram("@jne_id");
        infoKurirArrayList.add(kurir);

        kurir = new DataInfoKurir();
        kurir.setLogo(R.mipmap.logo_pos);
        kurir.setCall("-");
        kurir.setEmail("halopos@posindonesia.co.id");
        kurir.setFacebook("@posindonesia");
        kurir.setTwitter("@PosIndonesia");
        kurir.setInstagram("@posindonesia.ig");
        infoKurirArrayList.add(kurir);

        kurir = new DataInfoKurir();
        kurir.setLogo(R.mipmap.logo_tiki);
        kurir.setCall("(62-21) 314 0404");
        kurir.setEmail("tiki@tiki.id");
        kurir.setFacebook("@tiki.id");
        kurir.setTwitter("@IdTiki");
        kurir.setInstagram("@tiki_id");
        infoKurirArrayList.add(kurir);
    }
}
package com.example.cekongkir;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.cekongkir.ui.home.MainActivity;

import java.util.ArrayList;


public class ActivityBookmark extends AppCompatActivity {

    private ListView ListView01;
    private ListViewAdapter adapter;
    ArrayList<dataBookmark> bookmarkArrayList = new ArrayList<>();
    Menu menu;
    DataHelper dbcenter;
    protected Cursor cursor;

    public static ActivityBookmark ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);

        ma = this;
        dbcenter = new DataHelper(this);
        RefreshList();
    }

    public void RefreshList() {
        bookmarkArrayList.clear();
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM ongkir", null);
        while (cursor.moveToNext()){
            String asal = cursor.getString(0);
            String tujuan = cursor.getString(1);
            Integer berat = Integer.parseInt(cursor.getString(2));

            bookmarkArrayList.add(new dataBookmark(asal, tujuan, berat));
        }
        adapter = new ListViewAdapter(bookmarkArrayList, ActivityBookmark.this);
        ListView01 = (ListView) findViewById(R.id.listView1);
        ListView01.setAdapter(adapter);
        ListView01.setSelected(true);
        ListView01.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                final CharSequence[] dialogitem = {"Lihat Ongkos Kirim", "Hapus Data"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityBookmark.this);
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 0:
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra("asal", bookmarkArrayList.get(item).getAsal());
                                i.putExtra("tujuan", bookmarkArrayList.get(item).getTujuan());
                                i.putExtra("berat", bookmarkArrayList.get(item).getBerat());
                                startActivity(i);
                                break;
                            case 1:
                                Integer isDelete = dbcenter.deleteData(bookmarkArrayList.get(item).getAsal());
                                RefreshList();
                                break;
                        }
                    }
                });
                builder.create().show();
            }
        });
    }

}
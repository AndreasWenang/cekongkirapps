package com.example.cekongkir.ui.search;

import android.widget.EditText;

import com.example.cekongkir.data.city.DataCity;
import com.jakewharton.rxbinding3.widget.TextViewTextChangeEvent;

import java.util.List;

import com.example.cekongkir.data.city.ResponseCity;
import io.reactivex.observers.DisposableObserver;


public interface SearchCityContract {
    interface View{
        void onLoadingSearch(boolean loading);
        void onResultSearch(ResponseCity response);
        void onErrorSearch();

        void onResultInstantSearch(List<DataCity> data);

        void showMessage(String msg);
    }

    interface Presenter{
        void getCity();
        void instantSearch(EditText editText, List<DataCity> data);
        void searchCity(List<DataCity> data, String keyword);
        DisposableObserver<TextViewTextChangeEvent> observer(List<DataCity> data);
    }
}

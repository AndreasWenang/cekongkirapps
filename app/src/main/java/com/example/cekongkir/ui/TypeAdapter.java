package com.example.cekongkir.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.HolderData> {

    List<String> listData;
    LayoutInflater inflater;

    public TypeAdapter(List<String> listData, Context context, LayoutInflater inflater) {
        this.listData = listData;
        this.inflater = LayoutInflater.from(context);;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder{

        public HolderData(@NonNull View itemView) {
            super(itemView);
        }
    }
}

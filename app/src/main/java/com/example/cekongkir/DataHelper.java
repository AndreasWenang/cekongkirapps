package com.example.cekongkir;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "historyongkir.db";
    private static final String DATABASE_TABLE = "ongkir";

    private static final String arg0 = "asal";
    private static final String arg1 = "tujuan";
    private static final String arg2 = "berat";
    private static final String arg3 = "hasil";


    public DataHelper(Context context){
        super(context , DATABASE_NAME,null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + DATABASE_TABLE + "(" +
                arg0+ "TEXT," +
                arg1+ "TEXT," +
                arg2+ "INTEGER" +
                ")");
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL(" DROP TABLE IF EXISTS " + DATABASE_TABLE);

    }

    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(DATABASE_TABLE, arg0+" = ? ", new String[]{id});
    }

    public boolean updateData(String asal, String tujuan, Integer berat){
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(arg0,asal);
        values.put(arg1,tujuan);
        values.put(arg2,berat);

        db.update(DATABASE_TABLE,values,arg0+" = ? ", new String[]{asal});
        return false;
    }

}

package com.example.cekongkir;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class ListViewAdapter extends BaseAdapter {
    private List<dataBookmark> dataBookmarkList;
    private Context context;
    TextView asal, tujuan, berat;

    LinearLayout itemBookmark;

    public ListViewAdapter(List<dataBookmark> dataBookmarkList, Context context) {
        this.dataBookmarkList = dataBookmarkList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataBookmarkList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_item_bookmark, null);
        asal = v.findViewById(R.id.tv_asal);
        tujuan = v.findViewById(R.id.tv_tujuan);
        berat = v.findViewById(R.id.tv_berat);
        itemBookmark = v.findViewById(R.id.item_bookmark);

        asal.setText(dataBookmarkList.get(position).getAsal());
        tujuan.setText(dataBookmarkList.get(position).getTujuan());
        berat.setText(String.valueOf(dataBookmarkList.get(position).getBerat()));
        return v;
    }
}
